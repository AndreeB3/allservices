class Admin::CompaniesController < Admin::AdminController

  def index
    @companies = Company.all
  end
end
