Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #
  root 'welcome#index'

  namespace :admin do
    devise_for :users,
               #path: "auth",
               #path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'signup' },
               controllers: { registrations: 'admin/registrations', sessions: 'admin/sessions' }

    root 'welcome#index'
    resources :categories, :companies
  end
end
